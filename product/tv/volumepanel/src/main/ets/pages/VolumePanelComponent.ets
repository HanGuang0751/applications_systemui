/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Log from '../../../../../../../common/src/main/ets/default/Log'
import AbilityManager from '../../../../../../../common/src/main/ets/default/abilitymanager/abilityManager'
import StyleConfiguration, { VolumePanelComponentStyle } from '../../../../../../../features/volumepanelcomponent/src/main/ets/com/ohos/common/StyleConfiguration'
import Constants from '../../../../../../../features/volumepanelcomponent/src/main/ets/com/ohos/common/Constants'
import ViewModel from '../viewmodel/VolumePanelVM'
import VolumeWindowController from '../../../../../../../features/volumepanelcomponent/src/main/ets/com/ohos/common/VolumeWindowController'
import commonEvent from '@ohos.commonEvent';

const TAG = 'VolumePanel-VolumePanelComponent'

@Component
export default struct VolumePanelComponent {
  @StorageLink('VolumePanelMaxVolume') VolumePanelMaxVolume: number = Constants.DEFAULT_MAX_VOLUME
  @StorageLink('VolumePanelMinVolume') VolumePanelMinVolume: number = Constants.DEFAULT_MIN_VOLUME
  @StorageLink('VolumePanelVolumeValue') VolumePanelVolumeValue: number = Constants.DEFAULT_MIN_VOLUME
  @StorageLink('VolumePanelIsMute') VolumePanelIsMute: boolean = Constants.DEFAULT_MUTE_STATUS
  @State style: VolumePanelComponentStyle = StyleConfiguration.getVolumePanelComponentStyle()

  aboutToAppear() {
    Log.showInfo(TAG, 'this is my aboutToAppear');
    ViewModel.initViewModel();
    this.registerMuteCommonEvent();
  }

  aboutToDisappear() {
    Log.showInfo(TAG, 'aboutToDisappear');
  }

  registerMuteCommonEvent(){
    commonEvent.createSubscriber({events: ["KEY_MUTE"]})
      .then((commonEventSubscriber) => {
        console.log("=-= createSubscriber success, commonEventSubscriber: " +JSON.stringify(commonEventSubscriber));
        commonEvent.subscribe(commonEventSubscriber, (err, data) => {
          console.log("=-= createSubscriber success, commonEventSubscriber: " +JSON.stringify(commonEventSubscriber));
          if(err.code !== 0){
            console.log("=-= subscribe err, err: " +JSON.stringify(err));
            return;
          }
          this.onMuteBtnClick(null);
        })
      })
      .catch((err) => {
        console.log("=-= createSubscriber fail, err: "+ JSON.stringify(err));
      })
  }

  build() {
    Column() {
      Text(this.VolumePanelVolumeValue.toString())
        .fontSize(22)
        .fontWeight(FontWeight.Bold)
        .fontColor("#FFFFFF")
        .margin({top: '3.4%'})
        .height('8.3%')

      Slider({
        value: this.VolumePanelVolumeValue,
        min: this.VolumePanelMinVolume,
        max: this.VolumePanelMaxVolume,
        step: 1,
        style: SliderStyle.InSet,
        direction: Axis.Vertical,
        reverse: true
      })
        .margin({top: this.style.volumePanelSliderMarginTop, bottom: this.style.volumePanelSliderMarginBottom})
        .trackThickness(11)
        .blockColor("#FFFFFF")
        .trackColor("#37ffffff")
        .selectedColor("#FFFFFF")
        .onChange(this.onVolumeChange.bind(this))
        .flexGrow(1)
        .flexShrink(1)

      Image(this.getVolumeIcon(this.VolumePanelIsMute, this.VolumePanelVolumeValue, this.VolumePanelMaxVolume, this.VolumePanelMinVolume))
        .width('27.8%')
        .height('5.57%')
        .margin({bottom: 24})
        .fillColor($r('sys.color.ohos_id_color_activated'))

    }
    .width('100%')
    .height('100%')
    .borderRadius(5)
    .backgroundColor("#505050")
  }

  getVolumeIcon(isMute, volume, maxVolume, minVolume) {
    Log.showInfo(TAG, `getVolumeIcon, isMute: ${isMute} volume: ${volume} maxVolume: ${maxVolume} minVolume: ${minVolume}`);
    let icon
    if (isMute || volume === 0) {
      icon = $r('app.media.ic_public_mute')
    } else {
//      if (volume >= (((maxVolume - minVolume) / 3) * 2 + minVolume)) {
//        icon = $r('app.media.ic_public_sound_03')
//      } else if (volume >= ((maxVolume - minVolume) / 3 + minVolume)) {
//        icon = $r('app.media.ic_public_sound_02')
//      } else if (volume == minVolume) {
//        icon = $r('app.media.ic_public_sound_04')
//      } else {
//        icon = $r('app.media.ic_public_sound_01')
//      }
      icon = $r('app.media.tvos_ic_volume')
    }
    return icon
  }

  onVolumeChange(value: number, mode: SliderChangeMode) {
    Log.showInfo(TAG, `onVolumeChange, value: ${value} mode: ${mode}`);
    ViewModel.setVolume(value)
  }

  onMuteBtnClick(event: ClickEvent) {
    Log.showInfo(TAG, `onMuteBtnClick`);
    ViewModel.mute();
  }

  onSettingsBtnClick(event: ClickEvent) {
    Log.showInfo(TAG, `onSettingsBtnClick`);
    AbilityManager.startAbility(AbilityManager.getContext(AbilityManager.ABILITY_NAME_VOLUME_PANEL), {
      bundleName: 'com.ohos.settings',
      abilityName: 'com.ohos.settings.MainAbility',
    });
    VolumeWindowController.getInstance().setWindowState(false);
  }
}