import AbilityManager from '../../../../../../../common/src/main/ets/default/abilitymanager/abilityManager';
import CommonEvent from '@ohos.commonEvent';
import inputEventClient from '@ohos.multimodalInput.inputEventClient';
import keyCode from '@ohos.multimodalInput.keyCode';
import VolumeWindowController from '../../../../../../../features/volumepanelcomponent/src/main/ets/com/ohos/common/VolumeWindowController'
import Log from '../../../../../../../common/src/main/ets/default/Log';

const TAG = 'NavigationBarEventModel';

export class NavigationBarEventModel {
    private setupFirstDownTime: number = -1;

    constructor(){

    }

     distinctionKeyCode(keyEvent){
        let date = new Date()
        console.log(TAG, "keyEvent: " + JSON.stringify(keyEvent) + ' Date: ' + JSON.stringify(Date()) + 'Date.getTime: ' + JSON.stringify(date.getTime()));
        let code = keyEvent.keyCode
        let keyType = keyEvent.type

        if (code != keyCode.KeyCode.KEYCODE_MENU) {
            this.setupFirstDownTime = -1
        }
        switch (code) {
            case 2081:
                console.log(TAG, "case 1");
                if (keyType == 1) {
                    this.homeEvent();
                }
                break;
            case 2:
                console.log(TAG, "case 2");
                if (keyType == 1) {
                    this.sendBackEvent();
                }
                break;
            case 22:
                console.log(TAG, "case 22");
                if (keyType == 1) {
                    this.muteEvent();
                }
                break;
            case 2801:
                console.log(TAG, "need invoke KEY_SETUP");
                if (keyType == 1) {
                    this.setupEvent();
                }
                break;
            case keyCode.KeyCode.KEYCODE_MENU:
                console.log(TAG, "need invoke KEY_SETUP_MENU");

                if (keyType == 0) {
                    if (this.setupFirstDownTime == -1) {
                        this.setupFirstDownTime = date.getTime()
                    }else {
                        if (date.getTime() - this.setupFirstDownTime >= 3000) {
                            Log.showInfo(TAG, `distinctionKeyCode, time space: ${JSON.stringify(date.getTime() - this.setupFirstDownTime)} , isSettingsOnForeground: ${JSON.stringify(globalThis.isSettingsOnForeground)}`);
                            if (globalThis.isSettingsOnForeground == false) {
                                this.setupMenuEvent();
                            }
                        }
                    }
                }else if (keyType == 1) {
                    this.setupFirstDownTime = -1
                }
                break;
            default:
                break;
        }
         if (code != 2827 && code != 18 && code != 2067) {
             this.setupCompatibleKey();
         }
    }

    muteEvent(){
        VolumeWindowController.getInstance().setWindowState(true);
        CommonEvent.publish("KEY_MUTE", (err) => {
            if (err.code) {
                console.error("publish failed " + JSON.stringify(err));
            } else {
                console.info("publish");
            }
        });
    }

    sendBackEvent(){
        CommonEvent.publish("KEY_BACK", (err) => {
            if (err.code) {
                console.error("publish failed " + JSON.stringify(err));
            } else {
                console.info("publish");
            }
        });
    }

    homeEvent(){
        AbilityManager.startServiceExtensionAbility(AbilityManager.getContext(AbilityManager.ABILITY_NAME_NAVIGATION_BAR), {
            bundleName: "com.ohos.launcher",
            abilityName: "com.ohos.launcher.MainAbility"
        });

        CommonEvent.publish("KEY_HOME", (err) => {
            if (err.code) {
                console.error("publish failed " + JSON.stringify(err));
            } else {
                console.info("publish");
            }
        });
    }

    setupEvent(){
        AbilityManager.startAbility(AbilityManager.getContext(AbilityManager.ABILITY_NAME_NAVIGATION_BAR), {
            bundleName: "com.ohos.settings",
            abilityName: "com.ohos.settings.MainAbility"
        })

        CommonEvent.publish("KEY_SETUP", (err) => {
            if (err.code) {
                console.error("publish failed " + JSON.stringify(err));
            } else {
                console.info("publish");
            }
        });
    }

    setupMenuEvent(){
        AbilityManager.startServiceExtensionAbility(AbilityManager.getContext(AbilityManager.ABILITY_NAME_NAVIGATION_BAR), {
            bundleName: "com.ohos.settings",
            abilityName: "com.ohos.settings.menu.ServiceAbility"
        });

        CommonEvent.publish("KEY_SETUPMENU", (err) => {
            if (err.code) {
                console.error("publish failed " + JSON.stringify(err));
            } else {
                console.info("publish");
            }
        });
    }

    // 此键值为了规避返回后获取不到焦点的问题
    setupCompatibleKey() {
        setTimeout(()=>{
            inputEventClient.injectEvent({ KeyEvent:  {
                isPressed: true,
                keyCode: 2827,
                keyDownDuration: 1,
                isIntercepted: false
            } })
        }, 1000)
    }
}

let navigationBarEventModel = new NavigationBarEventModel();

export default navigationBarEventModel ;