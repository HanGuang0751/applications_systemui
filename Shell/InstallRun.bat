set systemui_replace_folder=\entry\tv\build\default\outputs\default\tv_entry-default-signed.hap

set systemui_navigationbar_replace_folder=\product\tv\navigationBar\build\default\outputs\default\tv_navigationBar-default-signed.hap

set systemui_volumepanel_replace_folder=\product\tv\volumepanel\build\default\outputs\default\tv_volumepanel-default-signed.hap

set hdc_std=hdc_std.exe

%hdc_std% shell mount -o remount,rw /

%hdc_std% shell rm -rf /data/*

%hdc_std% shell rm -rf /system/app/com.ohos.systemui/SystemUI-DropdownPanel.hap

%hdc_std% shell rm -rf /system/app/com.ohos.systemui/SystemUI-StatusBar.hap

%hdc_std% shell rm -rf /system/app/com.ohos.systemui/SystemUI-NotificationManagement.hap

%hdc_std% shell rm -rf /system/app/com.ohos.systemui/SystemUI-ScreenLock.hap

%hdc_std% file send %systemui_replace_folder% /system/app/com.ohos.systemui/SystemUI.hap

%hdc_std% file send %systemui_navigationbar_replace_folder% /system/app/com.ohos.systemui/SystemUI-NavigationBar.hap

%hdc_std% file send %systemui_volumepanel_replace_folder% /system/app/com.ohos.systemui/SystemUI-VolumePanel.hap

%hdc_std% shell reboot